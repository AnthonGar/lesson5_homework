#include "Menu.h"

//This func will move a givin shape.
//Input: the shape to move.
//Output: none.
void Menu::moveShapeMenu(vector<Shape*>& shapes, int index)
{
	double x, y;
	cout << "Please enter the X moving scale: ";
	cin >> x;
	cout << "Please enter the Y moving scale: ";
	cin >> y;

	//Remove from screen temporarily.
	removeShape(shapes, index, false);
	//Moving
	shapes[index]->move(Point(x, y));
	//Draw in new place.
	shapes[index]->draw(*_disp, *_board);
}

void Menu::addShapeMenu(vector<Shape*>& shapes)
{
	int choise
	bool invalidChoiseFlag = true;

	cout << "Enter " << 0 << " to add a circle." <<
	endl << "Enter " << 1 << " to add an arrow." <<
	endl << "Enter " << 2 << " to add a triangle." <<
	endl << "Enter " << 3 << " to add a rectangle." << endl;

	do
	{
		cin >> choice;
		//Check it the input is valid.
		if (choise >= 0 && choice <= 3)
			invalidChoiseFlag = false;

	} while (invalidChoiseFlag);

	switch (choice)
	{
	case 0:
		addCircle(shapes);
		break;
	case 1:
		addArrow(shapes);
		break;
	case 2:
		addTriangle(shapes);
		break;
	case 3:
		addRectangle(shapes);
		break;
	}
}

//This func will delete all the shapes peremenetly.
void Menu::deleteAllShapes(vector<Shape*>& shapes)
{
	for (int i = 0; i < shapes.size(); i++)
	{
		//Remove from screen.
		shapes[i]->clearDraw(*_disp, *_board);
	}
	//Clear vector.
	shapes.clear();
}

//this func will remove the wanted shape from the screen and draw the others.
//Input: the wanted shape to remove, bool - if we want to remove temporarily or permenetly.
void Menu::removeShape(vector<Shape*>& shapes, int index, bool deleteFromVector)
{
	for (int i = 0; i < shapes.size(); i++)
	{
		if (i == index)
		{
			//If its the shape we want to remove.
			shapes[i]->clearDraw(*_disp, *_board);
		}
		else
		{
			//Else, its a shape we want to keep.
			shapes[i]->draw(*_disp, *_board);
		}
	}

	if (deleteFromVector)
		shapes.erase(shapes.begin() + index);

}

//This func will take care of the whole modofication prccess.
void Menu::modShapesMenu(vector<Shape*>& shapes)
{
	bool invalidChoiseFlag = true;
	int choice;
	int index;
	//If there are no shapes
	if (shapes.size() == 0)
		return;
	//else
	for (int i = 0; i < shapes.size(); i++) {
		cout << "Enter " << i << " for " << shapes[i]->getName() <<"- Type: " << shapes[i]->getType() << endl;
	}

	do
	{
		cin >> index;
		//Check it the input is valid.
		if (index >= 0 && index <= shapes.size() + 1)
			invalidChoiseFlag = false;

	} while (invalidChoiseFlag);

	invalidChoiseFlag = true;

	cout << "Enter " << 0 << " to move the shape" <<
	endl << "Enter " << 1 << " to get its details." <<
	endl << "Enter " << 2 << " to remove the shape." << endl;
	
	do
	{
		cin >> choice;
		//Check it the input is valid.
		if (choise >= 0 && choice <= 2)
			invalidChoiseFlag = false;

	} while (invalidChoiseFlag);

	switch (choice)
	{
	case 0:
		moveShapeMenu(shapes, index);
		break;
	case 1:
		shapes[index]->printDetails();
		break;
	case 2:
		removeShape(shapes, index, true);
		break;
	}
}

//This func will print the main menu and return input from the user.
//Output: <userInput>.
int Menu::ChoicesMenu()
{
	bool invalidChoiseFlag = true;
	int choise;

	cout << "Enter " << 0 << " to add a new shape." <<
	endl << "Enter " << 1 << " to modify or get information from a current shape." <<
	endl << "Enter " << 2 << " to delete all of the shapes." <<
	endl << "Enter " << 3 << " to exit." << endl;


	do
	{
		cin >> choice;
		//Check it the input is valid.
		if (choise >= 0 && choice <= 3)
			invalidChoiseFlag = false;

	} while (invalidChoiseFlag);
	return choice;
}


//This func will add a rectangle to the "shapes" vector.
//Input: the vector of shapes.
void Menu::addRectangle(vector<Shape*>& shapes)
{
	double length, width, x, y;
	string name;

	cout << "Enter the X of the to left corner: " << endl;
	cin >> x;
	cout << "Enter the Y of the top left corner: " << endl;
	cin >> y;
	cout << "Please enter the length of the shape: " << endl;
	cin >> length;
	cout << "Please enter the width of the shape: " << endl;
	cin >> width;

	cout << "Enter the name of the shape: " << endl;
	cin >> name;

	Rectangle* rectangle = new Rectangle(Point(x, y), length, width, "Rectangle", name);
	shapes.push_back(rectangle);

	//Draw on the screen
	srand(time(NULL));
	rectangle->draw(*_disp, *_board);
}

//This func will add a triangle to the "shapes" vector.
//Input: the vector of shapes.
void Menu::addTriangle(vector<Shape*>& shapes)
{
	string name;
	vector<Point> p;
	double x, y;

	for (int i = 1; i < 4; i++) {
		cout << "Enter the X of point number: " << i << endl;
		cin >> x;
		cout << "Enter the Y of point number: " << i << endl;
		cin >> y;

		p.push_back(Point(x, y));
	}
	cout << "Enter the name of the shape: " << endl;
	cin >> name;

	Triangle* triangle = new Triangle(p[0], p[1], p[2], "Triangle", name);
	shapes.push_back(triangle);

	//Draw on the screen
	srand(time(NULL));
	triangle->draw(*_disp, *_board);
}


//This func will add an arrorw to the "shapes" vector.
//Input: the vector of shapes.
void Menu::addArrow(vector<Shape*>& shapes)
{
	string name;
	vector<Point> p;
	double x, y;

	cout << "Enter the X of the start point of the arrow: " << endl;
	cin >> x;
	cout << "Enter the Y of the start point of the arrow: " << endl;
	cin >> y;
	p[0] = new Point(x,y);

	cout << "Enter the X of the end point of the arrow: " << endl;
	cin >> x;
	cout << "Enter the Y of the end point of the arrow: " << endl;
	cin >> y;
	p[1] = new Point(x, y);

	cout << "Enter the name of the shape: " << endl;
	cin >> name;


	Arrow* arrow = new Arrow(p[0], p[1], "Arrow", name);
	shapes.push_back(arrow);

	//Draw on the screen
	srand(time(NULL));
	arrow->draw(*_disp, *_board);
}



//This func will add a circle to the "shapes" vector.
//Input: the vector of shapes.
void Menu::addCircle(vector<Shape*>& shapes)
{
	double x, y, radius;
	string name;

	cout << "Please enter X of the center of the circle: " << endl;
	cin >> x;
	cout << "Please enter Y of the center of the circle: " << endl;
	cin >> y;
	cout << "Please enter radius of the circle: " << endl;
	cin >> radius;
	cout << "Please enter the name of the shape:" << endl;
	cin >> name;

	Circle* circle = new Circle(Point(x, y), radius, "Circle", name);
	shapes.push_back(circle);

	//Draw on the screen
	srand(time(NULL));
	circle->draw(*_disp, *_board);
}





//-------Givin Funcs--------
Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

