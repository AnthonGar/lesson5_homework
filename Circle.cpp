#include "Circle.h"

//This func will move the center point of the circle.
//Input: different point.
//Output: none.
void Circle::move(const Point& other)
{
	_center += other;
}

//C-tor for the circle class
//Input: the center of the circle, the radius, the type of the shape and the name of the shape.
Circle::Circle(const Point & center, double radius, const string & type, const string & name) : Shape(name, type), _center(center), _radius(radius)
{

}

//D-tor for the circle class
Circle::~Circle()
{

}

//Getter function for the circle class. 
//Output: it returns the perimeter of the circle
double Circle::getPerimeter() const
{
	return 2 * PI * _radius;
}

//Getter function for the circle class. 
//Output: it returns the area of the circle.
double Circle::getArea() const
{
	return _radius * _radius * P;
}

//Getter function for the circle class. 
//Output: it returns the center point of the circle.
const Point& Circle::getCenter() const
{
	return _center;
}

//Getter function for the circle class. 
//Output: it returns the radius of the circle.
double Circle::getRadius() const
{
	return _radius;
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
}


