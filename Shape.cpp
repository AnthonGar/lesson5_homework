#include "Shape.h"
//Getter for the _type member.
string Shape::getType() const
{
	return _type;
}

//Getter for the _name memeber.
string Shape::getName() const
{
	return _name;
}

//This func will print a givin shape.
void Shape::printDetails() const
{
	cout << _type.c_str() << "\t" << _name.c_str() << "\t" << getArea() << "\t" << getPerimeter() << endl;
}

//C-tor for the shape class.
Shape::Shape(const string & name, const string & type) : _name(name), _type(type)
{

}