#include "Menu.h"

void main()
{
	Menu menu;
	vector<Shape*> shapes;

	int choice;	

	while (1)
	{
		choice = menu.ChoicesMenu();

		switch (choice)
		{
		case 0:
			menu.addShapeMenu(shapes);
			break;
		case 1:
			menu.modShapesMenu(shapes);
			break;
		case 2:
			menu.deleteAllShapes(shapes);
			break;
		case 3:
			exit(0);
			break;
		default:
			std::cout << "Invalid Input!" << endl;
			break;
		}
	}
}