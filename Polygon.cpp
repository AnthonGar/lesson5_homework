#include "Polygon.h"

//This func will calculate the perimemter of a polygon.
double Polygon::getPerimeter() const
{
	double perimeter = 0;

	for (int i = 0; i < _points.size() - 1; i++)
	{
		perimeter += _points[i].distance(_points[i + 1]);
	}

	//To also add the distance between the last and first points.
	perimeter += _points[_points.size() - 1].distance(_points[0]);

	return perimeter;
}

//This func will move the polygon.
void Polygon::move(const Point& other)
{
	for (int i = 0; i < _points.size(); i++)
	{
		_points[i] += other;
	}
}

//C-tor for the polygon class.
Polygon::Polygon(const string & type, const string & name): Shape(name, type)
{

}

//D-tor for the polygon class.
Polygon::~Polygon()
{

}

