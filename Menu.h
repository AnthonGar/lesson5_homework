#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();

	//Add
	void addCircle(vector<Shape*>& shapes);
	void addArrow(vector<Shape*>& shapes);
	void addTriangle(vector<Shape*>& shapes);
	void addRectangle(vector<Shape*>& shapes);
	
	//Menus
	int ChoicesMenu();//main menu...
	void addShapeMenu(vector<Shape*>& shapes);//Shape adding menu...
	void modShapesMenu(vector<Shape*>& shapes);//Modify menu...
	void moveShapeMenu(vector<Shape*>& shapes, int index);//Moving menu...

	//Deletes
	void removeShape(vector<Shape*>& shapes, int index, bool deleteFromVector);
	void deleteAllShapes(vector<Shape*>& shapes);




private: 

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

