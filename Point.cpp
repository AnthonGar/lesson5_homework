#include "Point.h"
#include <math.h>

//C-tor for the point class.
Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}
//Copy C-tor for the point class.
Point::Point(const Point& other)
{
	_x = other.getX();
	_y = other.getY();
}

//D-tor for the point class.
Point::~Point()
{

}

//+ operator, adds 2 points and returns a new point.
Point Point::operator+(const Point& other) const
{
	//New point
	Point point(_x + other.getX(), _y + other.getY());
	return point;
}

//+= operatoe, adds 2 points.
Point& Point::operator+=(const Point & other)
{
	_x += other.getX();
	_y += other.getY();

	return *this;
}

//Getter for the point class. return the _x member
double Point::getX() const
{
	return _x;
}

//Getter for the point class. return the _y member
double Point::getY() const
{
	return _y;
}

//Calculates the distance between 2 points.
//Input: 2 points (this and other).
//Output: distance
double Point::distance(const Point& other) const
{
	return sqrt(pow(_x - other.getX(), 2) + pow(_y - other.getY(), 2));
}
