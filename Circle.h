#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>
#define P 3.141592

class Circle : public Shape
{
public:
	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void move(const Point& other);

	Circle(const Point& center, double radius, const string& type, const string& name);
	~Circle();
	
	//Getters.
	const Point& getCenter() const;
	double getRadius() const;

	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

	// override functions if need (virtual + pure virtual)
private:

	Point _center;
	double _radius;
};