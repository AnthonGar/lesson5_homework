#include "Arrow.h"

//C-tor for the arrow class.
//Input: 2 points, name of the shape, type of the shape.
//Output: none.
Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name) : Shape(type, name)
{
	_p1 = a;
	_p2 = b;
}

//D-tor for the arrow class
Arrow::~Arrow()
{

}

//This func will move the arrow to a different point.
//Input: other point
//Output: none.
void Arrow::move(const Point & other)
{
	_p1 += other.getX();
	_p2 += other.getY();
}

//This func will calculate the distance from p1 to p2(arrow its just a line);
//Input: none.
//Output: the distance between the 2 points that draw the arrow.
double Arrow::getPerimeter() const
{
	return _p1.distance(_p2);
}

//This func will return 0... a line doesn't have volume.
double Arrow::getArea() const
{
	return 0.0;
}

//-----------Given Funcs--------------
void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}


